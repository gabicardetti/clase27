export default class CRUD {


    constructor(connection, table) {
        this.connection = connection;
        this.table = table;
    }

    async getAll() {
        try {
            const allObjects = await this.connection.from(this.table).select("*");
            return allObjects;
        } catch (e) {
            console.error(e)
        }
    }

    async save(object) {
        try {
            const storedObject = await this.connection.from(this.table).insert(object);
            object.id = storedObject[0];
            return object;
        } catch (e) {
            console.error(e)
        }
    }

    async getById(id) {
        try {
            const objects = await this.connection.from(this.table).select("*").where("id", "=", id);
            if(objects.lenght == 0)  return;
            return objects[0];
        } catch (e) {
            console.error(e)
        }
    }

    async deleteById(id) {
        try {
            const deletedObjects = await this.connection.from(this.table).where("id", "=", id).del();
            return deletedObjects;
        } catch (e) {
            console.error(e)
        }
    }

    async updateById(id, object) {
        try {
            const updatedObject = await this.connection.from(this.table).where("id", "=", id).update(object);
            if(!updatedObject) return;
            return object;
        } catch (e) {
            console.error(e)
        }
    }

}