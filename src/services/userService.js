import UserRepositoryMongo from "../repositories/userRepositoryMongo.js"
import bcrypt from "bcryptjs"

const repository = new UserRepositoryMongo();

export const getAllProducts = async () => repository.getAll();

export const createNewUser = async (username, password, profilePhoto) => {
  const hashedPassword = bcrypt.hashSync(password, 10);

  const user = {
    username, hashedPassword
  }
  if (profilePhoto)
    user.profilePhoto = profilePhoto

  const savedUser = await repository.save(user);
  return savedUser;
};

export const getUserByUsername = async (username) => {
  const user = await repository.getByUsername(username);
  return user;
};
